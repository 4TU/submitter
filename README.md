## Submitter

Submit apps to our homebrew repos! This form directly posts to [Dragonite](http://gitlab.com/4tu/dragonite), which pending approval makes its way to [Spinarak](http://gitlab.com/4tu/spinarak).

### Our Metadata Repos
- [switch.apps](http://gitlab.com/4tu/switch.apps) - Homebrew apps for Switch
- [wiiu.apps](http://gitlab.com/4tu/wiiu.apps) - Homebrew apps for Wii U
- [switch.themes](http://gitlab.com/4tu/switch.themes) - System themes for Switch
- [wiiu.plugins](http://gitlab.com/4tu/wiiu.plugins) - [WUPS](https://github.com/Maschell/WiiUPluginSystem) plugins for Wii U

The information in our metadata repos is available under a [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode) license.
